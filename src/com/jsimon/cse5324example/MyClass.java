package com.jsimon.cse5324example;

public class MyClass {
	
	// String to welcome the class
	private final String welcomeString = "Hello, World!";
	
	/**
	 * Multiply Function with special requirements
	 * X < 1000 and Y < 50
	 * 
	 * @param x 
	 * @param y
	 * @return x*y
	 */
	public int multiply(int x, int y){
		if( x > 999){
			throw new IllegalArgumentException("X should be less than 1000");
		}
		
		if( y > 50){
			throw new IllegalArgumentException("Y should be less than 50");
		}

		return x/y;
	}

	public String getWelcomeString() {
		return welcomeString;
	}
	
}
