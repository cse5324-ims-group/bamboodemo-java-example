package com.jsimon.cse5324example;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MyClassTest {

	@BeforeClass
	public static void testsetup(){
		
	}
	
	@AfterClass
	public static void testCleanup(){
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testExceptionIsThrown(){
		MyClass tester = new MyClass();
		tester.multiply(1000, 5);
	}
	
	@Test
	public void testMultiply() {
		MyClass tester = new MyClass();
		assertEquals("10 x 5 must be 50", 50, tester.multiply(10, 5));
	}
	
	@Test
	public void testWelcomeString(){
		MyClass tester = new MyClass();
		assertThat(tester.getWelcomeString(), containsString("Class"));
	}

}
